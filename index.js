var express = require('express');
var ejs = require('ejs');
var path = require('path');
var app = express();
app.use(express.static(__dirname + '/public')); //declares a static directory

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');

app.get('/', function(req, res){
	res.sendFile(__dirname + '/public/index.html');
});

var server = app.listen(app.get('port'), function(err) {
    if(err) throw err;

    var message = 'Server is running @ http://localhost:' + server.address().port;
    console.log(message);
 });
