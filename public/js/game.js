var c,
    scene,
    camera,
    controls,
    texture_placeholder,
    renderer;

var ball,cube,cube2,barrel;

var jsonLdr,objectLdr;
var weapon,plasmaBalls = [],emitter, plasmaBallsTimer = [];
// collision variables
var collisionArray = [], characterBox, cubeBox,cubeBox2,barrelBox, collisionArrayHealth = [];
var box1,box2,pBox,box3,playerPathBlocked,playerLastValidPosition,staticBox, isOnObject;
var currentCollisonObject;
var velocity = new THREE.Vector3();
var maxVelocity = 10;
//gltf loader
var gltf = null;
var gltfLoader;

var clock = new THREE.Clock();
var prevTime = Date.now();

// Monkey animation
var mesh;
var mixer;
var actions = {};

var male_animations=[] , idle_animation,walk_animation,run_animation,jump_animation;
var speed = 2; //1 walking, 2 running
var gltfModelList = [
  {
    name : "d",
    // url : "./models/gltf/d.gltf",
    url: "./models/gltf/anim3UV2.gltf",
    cameraPos: new THREE.Vector3(0, 5, 15),
    objectRotation: new THREE.Euler(0, 0, 0),
    addLights:true,
    shadows:true,
    extensions: ['glTF', 'glTF-Embedded', 'glTF-pbrSpecularGlossiness', 'glTF-Binary']
  },
]
function handle_load(geometry, materials) {

    //ANIMATION MESH
    var material = new THREE.MeshLambertMaterial({morphTargets: true});

    mesh = new THREE.Mesh(geometry, material);

    scene.add(mesh);
    // console.log(mesh);
    mesh.position.z = -10;
    mesh.scale.x = 3;
    mesh.scale.y = 3;
    mesh.scale.z = 3;



    //MIXER
    mixer = new THREE.AnimationMixer(mesh);

    var clip = THREE.AnimationClip.CreateFromMorphTargetSequence('talk', geometry.morphTargets, 30);
    actions.fall = mixer.clipAction(clip);
    actions.fall.setLoop(THREE.LoopRepeat);
    actions.fall.clampWhenFinished = true;
    actions.fall.setDuration(4).play();
}

function onMouseDown() {
  let plasmaBall = new THREE.Mesh(new THREE.SphereGeometry(0.3, 0.3, 0.3), new THREE.MeshBasicMaterial({
    color: "#fff"
  }));
  plasmaBall.position.copy(emitter.getWorldPosition()); // start position - the tip of the weapon
  
  var char = scene.getObjectByName('animations_male');
  plasmaBall.quaternion.copy(char.quaternion); // apply camera's quaternion
  scene.add(plasmaBall);
  plasmaBalls.push(plasmaBall);

  let timeDelay = new Date();
  timeDelay.setSeconds(timeDelay.getSeconds()+2);
  plasmaBallsTimer.push(timeDelay);
}

function setup() {
  init();
  animate();
  updateLVP();
}

function init() {
    jsonLdr = new THREE.JSONLoader();
    objectLdr =  new THREE.ObjectLoader();

    c = document.getElementById("myCanvas");
    renderer = new THREE.WebGLRenderer();
    renderer.gammaOutput = true; //helps with color rendering
    renderer.setSize( window.innerWidth, window.innerHeight);

    camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.5, 1000);
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xcccccc );
    scene.fog = new THREE.FogExp2( 0xcccccc, 0.002 );
    scene.add(camera);
    //camera.position.x = 10;
    camera.position.y = 5;
    camera.position.z = 5;
    
    //>>BACKGROUND
    texture_placeholder = document.createElement( 'canvas' );
    texture_placeholder.width = 256;
    texture_placeholder.height = 256;

    var context = texture_placeholder.getContext( '2d' );
    context.fillStyle = 'rgb( 200, 200, 200 )';
    context.fillRect( 0, 0, texture_placeholder.width, texture_placeholder.height );
        
    var materials = [

      loadTexture( '/textures/skybox/px.jpg' ), // right
      loadTexture( '/textures/skybox/nx.jpg' ), // left
      loadTexture( '/textures/skybox/py.jpg' ), // top
      loadTexture( '/textures/skybox/ny.jpg' ), // bottom
      loadTexture( '/textures/skybox/pz.jpg' ), // back
      loadTexture( '/textures/skybox/nz.jpg' )  // front

    ];

    var geometry = new THREE.BoxGeometry( 300, 300, 300, 7, 7, 7 );
    geometry.scale( - 1, 1, 1 );

    mesh = new THREE.Mesh( geometry, materials );
    scene.add( mesh );
    //<<BACKGROUND

    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.enableZoom = true;

    c.appendChild(renderer.domElement);

    // >> LIGTHS
    var ambientLight = new THREE.AmbientLight( 0x404040, 0.7 ); // soft white light
    scene.add( ambientLight );

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.7 );
    scene.add( directionalLight );
    // << LIGTHS

    //>> PLANE
      // floor
      var vertex = new THREE.Vector3();
      var color = new THREE.Color();
      var floorGeometry = new THREE.PlaneBufferGeometry( 500, 500, 100, 100 );
      floorGeometry.rotateX( - Math.PI / 2 );

      var loader = new THREE.TextureLoader();

      var texture = loader.load( 'models/grass.jpg', function ( texture ) {

          texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
          texture.offset.set( 0, 0 );
          texture.repeat.set( 80, 80 );
      } );

      var floorMaterial = new THREE.MeshPhongMaterial( {
        color: 0xffffff,
        specular:0x111111,
        shininess: 10,
        map: texture,
      } );

      // var floorMaterial = new THREE.MeshBasicMaterial( { vertexColors: THREE.VertexColors } );

      var floor = new THREE.Mesh( floorGeometry, floorMaterial );
      scene.add( floor );

    //<< PLANE
    //cube red
    var geometry = new THREE.BoxGeometry(5,10,5);
    var material = new THREE.MeshLambertMaterial({color: 'red', wireframe: false});
    cube = new THREE.Mesh(geometry,material);
    cube.position.x = -5;
    cube.position.y = 0.5;
    cube.name="cube";
    cube.castShadow = true;
    scene.add(cube);

    cubeBox = new THREE.BoxHelper(cube);
    // scene.add(cubeBox);

    // make bounding-box for mesh1
    cube.updateMatrix(); 
    cube.geometry.applyMatrix( cube.matrix );
    box1 = new THREE.Box3().setFromObject(cubeBox); //cube

    collisionArray.push(cube); //add static cube to collision array
    collisionArrayHealth.push(5);
    
    //>> cube green
    var geometry = new THREE.BoxGeometry(3,3,3);
    var material = new THREE.MeshLambertMaterial({color: 'green', wireframe: false});
    cube2 = new THREE.Mesh(geometry,material);
    cube2.position.x = -2;
    cube2.position.y = -0.2;
    cube2.name="cube2";
    scene.add(cube2);

    cubeBox2 = new THREE.BoxHelper(cube2);
    // scene.add(cubeBox2);

    // make bounding-box for mesh1
    cube2.updateMatrix(); 
    cube2.geometry.applyMatrix( cube2.matrix );
    box2 = new THREE.Box3().setFromObject(cubeBox2); //cube

    collisionArray.push(cube2); //add static cube to collision array
    collisionArrayHealth.push(5);
     //<< cube blue
    

     objectLdr.load('/models/first.json',function ( obj ) {
      obj.name = "first";
      obj.position.z = 5;
      obj.position.x = -6;
      obj.scale.x = 2.2;
      obj.scale.y = 2.2;
      obj.scale.z = 2.2;
      scene.add( obj );

      barrelBox = new THREE.BoxHelper(obj);
      scene.add(barrelBox);

      barrelBox.updateMatrix(); 
      barrelBox.geometry.applyMatrix(barrelBox.matrix );

      box3 = new THREE.Box3().setFromObject(barrelBox); //barrel

      collisionArray.push(obj); //add static cube to collision array
      collisionArrayHealth.push(5);
  });
    // objectLdr.load('/models/male.json',function ( obj ) {
    //   obj.name = "male";
    //   obj.position.z = 5;
    //   obj.position.x = -2;
    //   obj.scale.x = 0.2;
    //   obj.scale.y = 0.2;
    //   obj.scale.z = 0.2;
    //   obj.add(camera);
    //   scene.add( obj );
   // });

  // jsonLdr.load('/models/monkey_animated.json', handle_load); //MONKEY MODEL ANIMATION
  //jsonLdr.load('/models/ball.json', handle_load); //ball MODEL ANIMATION
  // jsonLdr.load('/models/model.json', handle_load);
  
  //>>GLTF OBJECT
  gltfLoader = new THREE.GLTFLoader();

  var man = gltfModelList[0];
  var url = man.url;

  gltfLoader.load( url, function(data) {
    console.log("data",data)
    gltf = data;

    var object = gltf.scene;

    object.traverse( function ( node ) {

      if ( node.isMesh ) node.castShadow = true;

    } );

    cameraIndex = 0;
    cameras = [];
    cameraNames = [];

    if (gltf.cameras && gltf.cameras.length) {

      var i, len = gltf.cameras.length;

      for (i = 0; i < len; i++) {

        var addCamera = true;
        var cameraName = gltf.cameras[i].parent.name || ('camera_' + i);

        if (addCamera) {
          cameraNames.push(cameraName);
          cameras.push(gltf.cameras[i]);
        }

      }

      updateCamerasList();
      switchCamera(1);

    } else {

      // updateCamerasList();
      // switchCamera(0);

    }

    male_animations = gltf.animations;

    if ( male_animations && male_animations.length ) {

      mixer = new THREE.AnimationMixer( object );
      console.log(male_animations);

      idle_animation = male_animations[0];
      walk_animation = male_animations[3];
      run_animation = male_animations[2];
      jump_animation = male_animations[1];

    }

    object.scale.x = 2;
    object.scale.y = 2;
    object.scale.z = 2;
    object.name = "animations_male";
    object.add(camera);

    weapon = new THREE.Mesh(new THREE.BoxGeometry(0.1, 0.1, 0.1), new THREE.MeshBasicMaterial({
      color: 0x5555ff
    }));
    weapon.position.set(object.position.x,object.position.y+1,object.position.z-0.2);
    object.add(weapon);
    emitter = new THREE.Object3D();
    emitter.position.set(object.position.x,object.position.y+1,object.position.z-0.2);
    object.add(emitter);

    characterBox = new THREE.BoxHelper(object);
      scene.add(characterBox);

      characterBox.updateMatrix(); 
      characterBox.geometry.applyMatrix(characterBox.matrix );

      pBox = new THREE.Box3().setFromObject(characterBox); //barrel

      playerPathBlocked = false; //for object collision
      playerLastValidPosition = object.position.clone();

    scene.add( object );
    onWindowResize();

  }, undefined, function ( error ) {

    console.error( error );

  } );
  //<<GLTF OBJECT

  window.addEventListener( 'resize', onWindowResize, false );
  window.addEventListener("mousedown", onMouseDown);
}

function updateLVP() { //update last valid position
  // var char = scene.getObjectByName("male");
  var char = scene.getObjectByName("animations_male");
  if (char){
    if(playerPathBlocked == false) {
      playerLastValidPosition = char.position.clone();
    } else {
      p = new THREE.Box3().setFromObject(char);
      let staticBox = currentCollisonObject;

      if (yAxisCollision(p,staticBox)) { //check collision on rotation
        if(p.max.y > staticBox.min.y && p.max.y < staticBox.max.y) {
          velocity.y += -10;
        } else {
          velocity.y += 10;
        }
      }

      if (xAxisCollision(p,staticBox)) { //check collision on rotation
        if(p.max.x > staticBox.min.x && p.max.x < staticBox.max.x) {
          char.position.x += -0.1;
        } else {
          char.position.x += 0.1;
        }

        if(velocity.y < 0) velocity.y = 0;
      }

      if (zAxisCollision(p,staticBox)) { //check collision on rotation
        if(p.max.z > staticBox.min.z && p.max.z < staticBox.max.z) {
          char.position.z += -0.1;
        } else {
          char.position.z += 0.1;
        }
        if(velocity.y < 0) velocity.y = 0;
      }
      playerPathBlocked = false; //set playerBlocked to false after collisions avoided
      // playerLastValidPosition = char.position.clone();
      // char.position.set(playerLastValidPosition.x,playerLastValidPosition.y,playerLastValidPosition.z);
    }
  }
  requestAnimationFrame(updateLVP);
}

function controlCharacter(charName) {
  var char = scene.getObjectByName(charName);
  var delta = clock.getDelta(); // seconds.
  var moveDistance = 5 * speed * delta; // 200 pixels per second
  var rotateAngle = Math.PI / 2* delta;
  
  // Set the velocity.x and velocity.z using the calculated time delta
  velocity.x -= velocity.x * 20.0 * delta;
  velocity.z -= velocity.z * 20.0 * delta;

  // As velocity.y is our "gravity," calculate delta
  velocity.y -= 9.8 * 5 * delta; // 100.0 = mass
  if (velocity.y > maxVelocity) velocity.y = maxVelocity;
  if (velocity.y < -maxVelocity) velocity.y = -maxVelocity; 

  if(char) {

    //camera related
    characterBox.update(); //update character box position
    var relativeCameraOffset = new THREE.Vector3(0,50,100);
    var cameraOffset = relativeCameraOffset.applyMatrix4( char.matrixWorld );
    //movement related
    if (playerPathBlocked == false) {

      if(char.is_jumping) {
        char.translateY( -velocity.y * delta);
      }

      //falling/gravitation related
      if(char.position.y <= 0) {
        char.position.y = 0;
        velocity.y = 0;
      } else if(isOnObject) {
        velocity.y = 0;
        // console.log("is on object");
      } else if(!char.is_jumping) {
        char.translateY( velocity.y * delta * 1.5); //increase velocity when falling
      }

      if (Key.isDown(Key.Q)) {
        velocity.x -= (speed * 100) * delta;
      } 
      if (Key.isDown(Key.E)) {
        velocity.x += (speed * 100) * delta;
      }
      if (Key.isDown(Key.W)) {
        velocity.z -= (speed * 100) * delta;
        if(Key.isDown(Key.SHIFT)) {
          speed=2;
          var prev_action = mixer.existingAction( walk_animation );
          prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";
          prev_action = mixer.existingAction( idle_animation );
          prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";

          mixer.clipAction( run_animation ).play();
        }else {
          speed=1;
          var prev_action = mixer.existingAction( run_animation );
          prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";
          prev_action = mixer.existingAction( idle_animation );
          prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";
          mixer.clipAction( walk_animation ).play();
        }
      }
      if (Key.isDown(Key.S)) {
        velocity.z += (speed * 100) * delta;
        var backwards = walk_animation;
        backwards.timeScale = -1;
        mixer.clipAction( backwards ).play();
      }
      if (Key.isDown(Key.A)) {
        char.rotateOnAxis( new THREE.Vector3(0,1,0), rotateAngle);
      }
      if (Key.isDown(Key.D)) {
        char.rotateOnAxis( new THREE.Vector3(0,1,0), -rotateAngle);
      }

      if (Key.isDown(Key.SPACE) && !char.is_jumping && !char.is_falling) {
 
        let jumpAnimation =  jump_animation;
        char.is_jumping = true;
        mixer.clipAction(jump_animation).play();
        setTimeout( function(){
          char.is_jumping = false;
          char.is_falling = true;
          setTimeout(function(){
            char.is_falling = false;
            var prev_action = mixer.existingAction( jump_animation );
            prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";
          }, jumpAnimation.duration * 1000/2.5)
        }, jumpAnimation.duration * 1000/2.5)
      }

      if ( ! (Key.isDown(Key.W) || Key.isDown(Key.S) || Key.isDown(Key.Q) || Key.isDown(Key.E))){
        var prev_action = mixer.existingAction( run_animation );
        prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";
        prev_action = mixer.existingAction( walk_animation );
        prev_action ? prev_action.isRunning() ? prev_action.stop() : "" : "";
        mixer.clipAction( idle_animation ).play();
      }

      char.translateX( velocity.x * delta);
      // char.translateY( velocity.y * delta);
      char.translateZ( velocity.z * delta);

    } else {

    }
  }
}

function animate() {
	renderer.render(scene, camera);
	requestAnimationFrame(animate);

  controlCharacter("animations_male");
  cubeBox.update();
  checkCollision();

  checkShootingCollision();

  if (mixer) {
    var time = Date.now();
    mixer.update((time - prevTime) * 0.001);
    prevTime = time;
  }

}

function checkShootingCollision() {
  let delta = clock.getDelta(); // seconds.
  if(plasmaBalls.length) {
    for ( var i=0 ; i< collisionArray.length; i++) {
      let now = new Date();
      staticBox = new THREE.Box3().setFromObject(collisionArray[i]);
   
      plasmaBalls.forEach( (b,index) => {

        let ball = new THREE.Box3().setFromObject(plasmaBalls[0]);

        if (ball.intersectsBox(staticBox)) {

          if(collisionArrayHealth[i] > 0){
            collisionArrayHealth[i]--;
          } else {
            scene.remove(collisionArray[i]);
            collisionArray.splice(i,1);
            collisionArrayHealth.splice(i,1);
          }
        }

        if(plasmaBallsTimer[index] < now || ball.intersectsBox(staticBox)) { //remove
          scene.remove(plasmaBalls[0]); //
          plasmaBalls[0].geometry.dispose();
          plasmaBalls[0].material.dispose();
          plasmaBalls[0] = undefined;

          plasmaBalls.splice(0,1);
          plasmaBallsTimer.splice(0,1);
        } else {
          b.translateZ(-800 * delta); // move along the local z-axis
        }
      });
    }
  }
}

function checkCollision() {
  if(box1 && pBox && box3) {
    p = new THREE.Box3().setFromObject(scene.getObjectByName("animations_male"));
    playerPathBlocked = false;
    isOnObject = false;

    for ( var i=0 ; i< collisionArray.length; i++) {
      staticBox = new THREE.Box3().setFromObject(collisionArray[i]);

      //>>> check if player is on object
      if( xAxisCollision(p, staticBox) && zAxisCollision(p, staticBox) && (p.min.y - staticBox.max.y <= 0.1 && p.min.y > staticBox.max.y) ) {
        // console.log("on  object");
        isOnObject = true;
      }

      if(p.intersectsBox(staticBox) && !isOnObject) {
          currentCollisonObject = staticBox;
          playerPathBlocked = true;
      }

      delete staticBox;
    }
  
  }

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

function loadTexture( path ) {

  var texture = new THREE.Texture( texture_placeholder );
  var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 } );

  var image = new Image();
  image.onload = function () {

    texture.image = this;
    texture.needsUpdate = true;

  };
  image.src = path;

  return material;

}

function yAxisCollision(obj1,obj2) {
  return ( (obj1.max.y > obj2.min.y && obj1.min.y < obj2.max.y) || (obj2.max.y > obj1.min.y && obj2.min.y < obj1.max.y) );
}

function xAxisCollision(obj1, obj2) {
  return ( (obj1.min.x > obj2.max.x && obj1.max.x < obj2.min.x) || (obj1.min.x < obj2.max.x && obj1.max.x > obj2.min.x) );
}

function zAxisCollision(obj1, obj2) {
  return ( (obj1.min.z > obj2.max.z && obj1.max.z < obj2.min.z) || (obj1.min.z < obj2.max.z && obj1.max.z > obj2.min.z) );
}

